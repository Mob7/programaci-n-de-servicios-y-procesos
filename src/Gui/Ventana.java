package Gui;

import Tareas.Descarga;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by sergio on 29/11/2015.
 */
public class Ventana {
    private JPanel panel1;
    private JTextField tfNombre;
    private JButton btAnadir;
    private JButton btRutaPorDefecto;
    private JButton btHistorial;
    private JPanel Paneles;
    private JLabel lbEstadoRuta;

    private static File rutaFichero;

    private File rutaPorDefecto = (new File(System.getProperty("user.home")));

    private ArrayList<DescargaFichero> listaPaneles;


    public Ventana(){

        Thread hiloSplash = new Thread(new Runnable() {

            @Override
            public void run(){
                SplashScreen splash = new SplashScreen();
                splash.mostrar();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie){
                    ie.printStackTrace();
                }
                splash.ocultar();
            }

        });
        hiloSplash.start();

        try{
            hiloSplash.join();
        } catch (InterruptedException ie){
            ie.printStackTrace();
        }


        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        listaPaneles = new ArrayList<DescargaFichero>();

        rutaFichero = rutaPorDefecto;

        btAnadir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                anadirDescarga();
            }
        });

        btRutaPorDefecto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elegirRutaPorDefecto();
            }
        });

        btHistorial.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                irAlHistorial();
            }
        });

        lbEstadoRuta.setText("Elija ruta por defecto");

    }

    private void anadirDescarga(){

        DescargaFichero panel = new DescargaFichero(tfNombre.getText(), rutaFichero);
        tfNombre.setText("");
        Paneles.add(panel.panelDescarga);
        Paneles.revalidate();
        listaPaneles.add(panel);

    }

    private void elegirRutaPorDefecto(){

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.showSaveDialog(null);
        rutaFichero = fileChooser.getSelectedFile();

        lbEstadoRuta.setText("Ruta establecida");

    }

    private void irAlHistorial() {

        try {

            Runtime.getRuntime().exec("notepad historial.txt");

        } catch (IOException ex){
            ex.printStackTrace();
        }

    }

}
