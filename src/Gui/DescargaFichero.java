package Gui;

import Tareas.Descarga;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

/**
 * Created by sergio on 29/11/2015.
 */
public class DescargaFichero {
    JPanel panelDescarga;
    private JTextField tfUrl;
    private JButton btRuta;
    private JProgressBar pbDescarga;
    private JLabel lbProgreso;
    private JButton btEliminar;
    private JButton btCancelar;
    private JButton btIniciar;
    private JLabel lbDescarga;
    private JButton btBorrarArchivo;
    private JLabel lbTamanoDescarga;

    private String nombreDescarga;
    private File rutaFichero;

    private Descarga thisDescarga;


    public DescargaFichero(String nombreDescarga, File rutaFichero){
        this.nombreDescarga = nombreDescarga;
        lbDescarga.setText(nombreDescarga);
        this.rutaFichero = rutaFichero;

        btRuta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rutaFichero();
            }
        });

        btIniciar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                iniciarFichero();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelarFichero();
            }
        });

        btEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarSwing();
            }
        });

        btBorrarArchivo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                borrarArchivo();
            }
        });

    }

    private void rutaFichero(){

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.showSaveDialog(null);
        rutaFichero = fileChooser.getSelectedFile();

    }

    private void iniciarFichero(){

        try {
            thisDescarga = new Descarga(tfUrl.getText(), rutaFichero.getAbsolutePath(), nombreDescarga);
            thisDescarga.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent event) {
                        if (event.getPropertyName().equals("progress")){
                            pbDescarga.setValue((Integer) event.getNewValue());
                            lbProgreso.setText(String.valueOf((Integer) event.getNewValue())+"% completado");
                            lbTamanoDescarga.setText(String.valueOf(thisDescarga.getTamanoTotalMB())+" MBs.");
                        }
                    }
                });

            } catch (Exception e){
                if (e instanceof MalformedURLException){
                    JOptionPane.showMessageDialog(null, "La URL no es correcta", "Descargar fichero", JOptionPane.ERROR_MESSAGE);
                } else if (e instanceof FileNotFoundException){
                    JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar fichero", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Ruta no especificada, por favor, selecciona una ruta", "Descargar fichero", JOptionPane.ERROR_MESSAGE);
                    e.printStackTrace();
                }
            }

        thisDescarga.execute();

    }

    private void cancelarFichero(){

        thisDescarga.cancel(true);
        JOptionPane.showMessageDialog(null, "La descarga ha sido cancelada", "Descarga cancelada", JOptionPane.INFORMATION_MESSAGE);
        btBorrarArchivo.setEnabled(true);

    }

    private void borrarArchivo(){

        int respuesta = MensajeDeConfirmacion("¿Quieres eliminar la descarga?");

        if (respuesta == JOptionPane.YES_OPTION) {

            thisDescarga.eliminarArchivo();

        } else {

            return;

        }
    }

    private void eliminarSwing(){

        int respuestaEliminar = MensajeDeConfirmacion("¿Seguro que quieres eliminar la descarga de la ventana?");

        if (respuestaEliminar == JOptionPane.YES_OPTION) {

            panelDescarga.setVisible(false);

        } else {

            return;

        }

    }

    public int MensajeDeConfirmacion(String Mensaje){
        return JOptionPane.showConfirmDialog(null, Mensaje);
    }



}
