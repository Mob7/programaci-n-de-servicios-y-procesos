package Gui;

import javax.swing.*;

/**
 * Created by sergio on 02/12/2015.
 */
public class SplashScreen {
    private JPanel panel1;

    private JFrame frame;

    public SplashScreen(){
        frame = new JFrame();
        frame.getContentPane().add(panel1);
        frame.setUndecorated(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    public void mostrar(){
        frame.setVisible(true);
    }

    public void ocultar(){
        frame.setVisible(false);
    }
}
