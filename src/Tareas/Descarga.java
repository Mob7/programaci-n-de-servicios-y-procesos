package Tareas;

import javax.swing.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

/**
 * Created by sergio on 29/11/2015.
 */
public class Descarga extends SwingWorker<Void, Integer> {

    private String urlFichero;
    private String rutaFichero;
    private String nombreDescarga;
    private int progresoTotalDescarga;
    private String Separador = System.getProperty("file.separator");
    private float tamanoTotalMB;
    private String extension;

    public float getTamanoTotalMB() {
        return tamanoTotalMB;
    }



    public Descarga(String urlFichero, String rutaFichero, String nombreDescarga) {

        this.urlFichero = urlFichero;
        this.rutaFichero = rutaFichero;
        this.nombreDescarga = nombreDescarga;
        extension = "."+urlFichero.substring(urlFichero.length() - 3);


    }

    @Override
    protected Void doInBackground() throws MalformedURLException, FileNotFoundException, IOException {

        URL url = new URL(urlFichero);
        URLConnection conexion = url.openConnection();
        int tamanoFichero = conexion.getContentLength();

        tamanoTotalMB = tamanoFichero / (1024*1024);

        InputStream is = url.openStream();
        FileOutputStream fos = new FileOutputStream(rutaFichero + Separador + nombreDescarga + extension);
        byte[] bytes = new byte[2048];
        int longitud = 0;
        int progresoDescarga = 0;

        while ((longitud = is.read(bytes)) != -1) {

            fos.write(bytes, 0, longitud);

            progresoDescarga += longitud;
            setProgress((int) (progresoDescarga * 100 / tamanoFichero));
            progresoTotalDescarga = progresoDescarga * 100 / tamanoFichero;

            if (isCancelled()) {
                is.close();
                fos.close();
                almacenarEnElHistorial(false);
                return null;
            }
        }

            is.close();
            fos.close();
            setProgress(100);
            almacenarEnElHistorial(true);

            return null;

        }



    private void almacenarEnElHistorial(boolean sino) {

            try {
                PrintWriter documento = new PrintWriter
                        (new FileWriter("historial.txt", true));
                documento.println(" ");
                documento.println("** "+ nombreDescarga +" **");
                documento.println("> Nombre del archivo: "+ nombreDescarga+extension);
                documento.println("> Ruta del archivo: "+ rutaFichero);
                if (sino==true){
                    documento.println("> Estado de la descarga: Completada (100%)");
                    documento.println(" ");
                    documento.println("______________________________________________________");
                } else {
                    documento.println("> Estado de la descarga: Cancelada.");
                    documento.println("> Porcentaje descargado: "+ progresoTotalDescarga +"%");
                    documento.println(" ");
                    documento.println("______________________________________________________");
                }
                documento.close();
            } catch (IOException e){
                System.out.println("Error en el archivo");
            }

        }

    public void eliminarArchivo(){

        File rutaFinal = (new File(rutaFichero + Separador + nombreDescarga + extension));
        try {

            Files.deleteIfExists(rutaFinal.toPath());
            JOptionPane.showMessageDialog(null, "La descarga ha sido eliminada", "Descarga eliminada", JOptionPane.INFORMATION_MESSAGE);

        } catch (NoSuchFileException nsfe){
            nsfe.printStackTrace();
        } catch (DirectoryNotEmptyException dnee){
            dnee.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }

    }

}
